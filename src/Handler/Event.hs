{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ViewPatterns #-}

module Handler.Event where

import Import as I
import Network.Wreq
import Control.Lens
import Data.List.Split
import System.Random (randomRIO)
import Prelude ((!!))
import Control.Concurrent (forkIO)

import GHC.Conc.Sync (ThreadId)

postEventR :: Handler Value
postEventR = do
    e <- (requireJsonBody :: Handler EventWrapper)
    master <- getYesod
    let token1 = slackToken $ appSettings $ master
    runInnerHandler <- handlerToIO
    if token1 == (token e)
      then do
        let responseUrl = slackWebHookUrl $ appSettings $ master
        _ <- replyAsync runInnerHandler responseUrl e
        returnJson $ getResponse e
      else permissionDenied "Bad token"

replyAsync :: MonadIO m => (HandlerFor App () -> IO ()) -> Text -> EventWrapper -> m ThreadId
replyAsync runInnerHandler responseUrl e = do
    liftIO $ forkIO $ runInnerHandler $ do
        saveEventWrapper e
        quotes <- liftIO getQuotes
        r <- liftIO $ getAction quotes e
        case r of
          (Just body) -> do
            liftIO $ messageSlack responseUrl body
          Nothing -> return ()

getQuotes :: IO [Text]
getQuotes = do
  content <- readFile "internalConfig/quotes.txt"
  return $ map pack $ splitOn "\n" $ unpack $ decodeUtf8 content

messageSlack :: Text -> ResponseBody -> IO ()
messageSlack url body = do
     r <- post (unpack url) (toJSON body)
     let resp = unpack $ decodeUtf8 (r ^. Network.Wreq.responseBody)
     putStrLn $ pack $ show resp
     return ()

-- Immediate response to respond with
getResponse :: EventWrapper -> ResponseBody
getResponse  (EventWrapper "url_verification" _ (Just challenge') _) = ChallengeResponse challenge'
getResponse  (EventWrapper "url_verification" _ Nothing _) = Simple { status = "challenge missing" }
getResponse (EventWrapper "event_callback" _ _ _) = Simple { status = "OK" }
getResponse (EventWrapper _ _ _ _) = Simple { status = "OK" }

-- Action to take based on the received event
getAction :: [Text] -> EventWrapper -> IO (Maybe ResponseBody)
getAction _ (EventWrapper "url_verification" _ _ _) = return Nothing
getAction quotes (EventWrapper "event_callback" _ _ (Just e)) = getEventAction quotes e
getAction _ (EventWrapper _ _ _ _) = return Nothing

getEventAction :: [Text] -> Event -> IO (Maybe ResponseBody)
getEventAction quotes (Event "message" _ _ _ (I.stripPrefix "sbot " . toLower -> Just msgText) _ _ _ _ _) = do
  response <- (respondToText quotes msgText)
  return $ Just (Message { text = response })
getEventAction _ (Event "app_mention" _ _ Nothing _ _ _ _ _ _) = return $ Just (Message { text = "I'm here" })
getEventAction _ _ = return Nothing

respondToText :: [Text] -> Text -> IO Text
respondToText _ (I.stripPrefix "say " -> Just rest) = return rest
respondToText _ "hey" = return "hey"
respondToText _ "rock on" = return "HELL YEAH!!!"
respondToText quotes _ = takeRandom quotes

takeRandom :: [b] -> IO b
takeRandom xs = fmap (xs !!) $ randomRIO (0, length xs - 1)

saveEventWrapper :: EventWrapper -> HandlerFor App ()
saveEventWrapper eventWrapper = do
  _ <- case event eventWrapper of
    Just e -> do
      res <- runDB $ insert $ toDBEvent e
      runDB $ insert $ toDBEventWrapper eventWrapper (Just res)
    Nothing -> runDB $ insert $ toDBEventWrapper eventWrapper Nothing
  return ()

saveEvent :: Event  -> HandlerFor App (Key DBEvent)
saveEvent e = runDB $ insert $ toDBEvent e

toDBEventWrapper :: EventWrapper -> Maybe (Key DBEvent) -> DBEventWrapper
toDBEventWrapper (EventWrapper bodyType1 token1 challenge1 _) id' =
  DBEventWrapper bodyType1 token1 challenge1 id'

toDBEvent :: Event -> DBEvent
toDBEvent (Event eventType1 userId1 botId1 subtype1 eventText1 clientMsgId1 ts1 channel1
      eventTs1 channelType1) =
  DBEvent eventType1 userId1 botId1 subtype1 eventText1 clientMsgId1 ts1 channel1
      eventTs1 channelType1
