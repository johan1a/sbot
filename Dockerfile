FROM johan1a/sbot-base

RUN rm -rf /app/config
COPY internalConfig /app/internalConfig
WORKDIR /app

ENTRYPOINT ["/usr/local/bin/sbot"]

