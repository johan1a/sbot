{-# LANGUAGE PackageImports #-}
import "sbot" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
