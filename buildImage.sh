#!/bin/sh
stack image container --docker || exit 1
TAG=$(git rev-parse HEAD)
docker build -t johan1a/sbot:${TAG} .
docker build -t johan1a/sbot:latest .
